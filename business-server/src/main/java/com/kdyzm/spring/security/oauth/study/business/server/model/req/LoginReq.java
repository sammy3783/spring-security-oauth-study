package com.kdyzm.spring.security.oauth.study.business.server.model.req;

import lombok.Data;

/**
 * @author kdyzm
 */
@Data
public class LoginReq {

    private String username;

    private String password;
}
